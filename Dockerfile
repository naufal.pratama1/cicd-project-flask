FROM python:3.8.0-slim
WORKDIR /app
ADD . ./app
RUN pip install --upgrade pip
RUN pip install Flask-SQLAlchemy
RUN pip install gunicorn
# RUN pip freeze > requirements.txt
ENV NAME anon
CMD gunicorn --bind :$PORT app:app  --reload
